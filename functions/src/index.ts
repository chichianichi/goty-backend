import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

import * as express from "express";
import * as cors from "cors";

const serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

// esto crea la referencia a la base de datos
const db = admin.firestore();

// // Start writing functions
// // https://firebase.google.com/docs/functions/typescript
//
export const helloWorld = functions.https.onRequest((request, response) => {
  const nombre = request.query.nombre || "Sin nombre";
  response.json({
    nombre,
  });
});

export const getGOTY = functions.https.onRequest(async (request, response) => {
  const gotyRef = db.collection("goty");
  const docsSnap = await gotyRef.get();
  const games = docsSnap.docs.map((doc) => doc.data());
  response.json(games);
});

const app = express();
app.use(cors({origin: true}));

app.get("/goty", async (req, res) => {
  const gotyRef = db.collection("goty");
  const docsSnap = await gotyRef.get();
  const games = docsSnap.docs.map((doc) => doc.data());
  res.json(games);
});

app.post("/goty/:id", async (req, res) => {
  const id = req.params.id;
  // vamos a revisar si existe la entrada con el id
  const gameRef = db.collection("goty").doc(id);
  const gameSnap = await gameRef.get();

  if (!gameSnap.exists) {
    res.status(404).json({
      ok: false,
      message: "id not found :  " + id,
    });
  } else {
    const before = gameSnap.data() || {votes: 0};
    await gameRef.update({
      votes: before.votes + 1,
    });

    res.json({
      ok: true,
      message: `Thanks for your vote to ${before.name}`,
    });
  }
});

export const api = functions.https.onRequest(app);
